package com.ruoyi.common.mybatis.web.domain;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

public class Condition {
	
	public Condition() {}
	
	public static <T> QueryWrapper<T> getQueryWrapper(T entity) {
        return new QueryWrapper<T>(entity);
    }

}
