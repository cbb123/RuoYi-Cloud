package com.tian.app;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.ruoyi.common.mybatis.annotation.EnableMybatisConfig;
import com.ruoyi.common.security.annotation.EnableCustomConfig;
import com.ruoyi.common.security.annotation.EnableRyFeignClients;
import com.ruoyi.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 小程序服务
 * 
 * @author ruoyi
 */
@EnableCustomConfig
@EnableMybatisConfig
@EnableCustomSwagger2   
@EnableRyFeignClients
@SpringBootApplication
@MapperScan("com.tian.**.mapper.**")
public class TianAppApplication {
	
	public static void main(String[] args)
    {
        SpringApplication.run(TianAppApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  小程序模块启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                " .-------.       ____     __        \n" +
                " |  _ _   \\      \\   \\   /  /    \n" +
                " | ( ' )  |       \\  _. /  '       \n" +
                " |(_ o _) /        _( )_ .'         \n" +
                " | (_,_).' __  ___(_ o _)'          \n" +
                " |  |\\ \\  |  ||   |(_,_)'         \n" +
                " |  | \\ `'   /|   `-'  /           \n" +
                " |  |  \\    /  \\      /           \n" +
                " ''-'   `'-'    `-..-'              ");
    }

}
