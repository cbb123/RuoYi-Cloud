package com.tian.app.controller;

import java.util.Arrays;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.mybatis.web.domain.Condition;
import com.ruoyi.common.security.annotation.PreAuthorize;
import com.tian.app.domain.AppUser;
import com.tian.app.service.IAppUserService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.SecurityUtils;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 用户信息Controller
 * 
 * @author jerry
 * @date 2021-07-12
 */
@RestController
@RequestMapping("/user")
public class AppUserController extends BaseController
{
    @Autowired
    private IAppUserService appUserService;

    /**
     * 查询用户信息列表
     */
    @PreAuthorize(hasPermi = "app:user:list")
    @GetMapping("/list")
    public TableDataInfo list(AppUser appUser)
    {
        startPage();
        List<AppUser> list = appUserService.list(Condition.getQueryWrapper(appUser));
        return getDataTable(list);
    }

    /**
     * 导出用户信息列表
     */
    @PreAuthorize(hasPermi = "app:user:export")
    @Log(title = "用户信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AppUser appUser) throws IOException
    {
        List<AppUser> list = appUserService.list(Condition.getQueryWrapper(appUser));
        ExcelUtil<AppUser> util = new ExcelUtil<AppUser>(AppUser.class);
        util.exportExcel(response, list, "用户信息数据");
    }

    /**
     * 获取用户信息详细信息
     */
    @PreAuthorize(hasPermi = "app:user:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(appUserService.getById(id));
    }
    
    /**
     * 获取用户信息详细信息
     */
    @GetMapping(value = "/info")
    public AjaxResult getAppUserInfo()
    {
    	Long userId = SecurityUtils.getUserId();
        return AjaxResult.success(appUserService.getById(userId));
    }

    /**
     * 新增用户信息
     */
    @PreAuthorize(hasPermi = "app:user:add")
    @Log(title = "用户信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AppUser appUser)
    {
        return toAjax(appUserService.save(appUser));
    }

    /**
     * 修改用户信息
     */
    @PreAuthorize(hasPermi = "app:user:edit")
    @Log(title = "用户信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AppUser appUser)
    {
        return toAjax(appUserService.updateById(appUser));
    }

    /**
     * 删除用户信息
     */
    @PreAuthorize(hasPermi = "app:user:remove")
    @Log(title = "用户信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(appUserService.removeByIds(Arrays.asList(ids)));
    }
}
