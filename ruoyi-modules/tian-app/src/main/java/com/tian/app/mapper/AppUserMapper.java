package com.tian.app.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tian.app.domain.AppUser;
import com.tian.app.domain.vo.AppUserVO;

/**
 * 用户信息Mapper接口
 * 
 * @author jerry
 * @date 2021-07-12
 */
public interface AppUserMapper extends BaseMapper<AppUser> {

	List<AppUserVO> selectAppUserList(AppUser appUser);
   
}
