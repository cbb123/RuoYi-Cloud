package com.tian.app.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tian.app.domain.AppUser;
import com.tian.app.domain.vo.AppUserVO;

/**
 * 用户信息Service接口
 * 
 * @author jerry
 * @date 2021-07-12
 */
public interface IAppUserService extends IService<AppUser> {

	List<AppUserVO> selectAppUserList(AppUser appUser);
   
}
