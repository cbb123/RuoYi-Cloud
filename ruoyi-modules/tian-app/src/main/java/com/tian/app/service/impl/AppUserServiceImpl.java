package com.tian.app.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import com.tian.app.mapper.AppUserMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tian.app.domain.AppUser;
import com.tian.app.domain.vo.AppUserVO;
import com.tian.app.service.IAppUserService;

/**
 * 用户信息Service业务层处理
 * 
 * @author jerry
 * @date 2021-07-12
 */
@Service
public class AppUserServiceImpl extends ServiceImpl<AppUserMapper, AppUser> implements IAppUserService {

	@Override
	public List<AppUserVO> selectAppUserList(AppUser appUser) {
		return baseMapper.selectAppUserList(appUser);
	}
    
}
